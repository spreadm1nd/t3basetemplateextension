$(document).ready(function() {
    $('.lightbox').magnificPopup({
      type:'image',
      tClose: 'Schließen (Esc)', // Alt text on close button
      tLoading: 'Bilder werden geladen ...', // Text that is displayed during loading. Can contain %curr% and %total% keys
      gallery: {
        enabled: true,
        tCounter: '<span class="mfp-counter">%curr% von %total%</span>',
        tPrev: 'Vorheriges (Linke Pfeil Taste)', // title for left button
        tNext: 'Nächstes (Rechte Pfeil Taste)', // title for right button
      }
    });
});