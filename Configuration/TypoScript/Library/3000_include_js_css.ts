page {
	includeCSS. {
		css = {$resDir}/Public/Css/00.styles.min.css
	}
		
	includeJS{
	#	jquery = {$resDir}/Public/ScriptMin/jquery-2.2.4.min.js
		jquery = {$resDir}/Public/ScriptMin/jquery-3.3.1.min.js
	#	modernizr = {$resDir}/Public/ScriptMin/modernizr.custom.79639.js
	}
		
	includeJSFooter {
	    js_include = {$resDir}/Public/ScriptMin/js_include.min.js
	}

}
