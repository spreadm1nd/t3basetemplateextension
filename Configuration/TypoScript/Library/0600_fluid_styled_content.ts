#########################################################################
###################### Eigene Templates für Fluid styled Content
#########################################################################

lib.fluidContent {
	templateRootPaths {
		200 = {$resDir}/Private/Templates/Fluidstyledcontent/
	}
	
	layoutRootPaths {
		200 = {$resDir}/Private/Layouts/Fluidstyledcontent/
	}
	
	partialRootPaths {
		200 = {$resDir}/Private/Partials/Fluidstyledcontent/
	}
}


lib.contentElement {
	templateRootPaths {
		200 = {$resDir}/Private/Templates/Fluidstyledcontent/
	}
	
	layoutRootPaths {
		200 = {$resDir}/Private/Layouts/Fluidstyledcontent/
	}
	
	partialRootPaths {
		200 = {$resDir}/Private/Partials/Fluidstyledcontent/
	}
}
