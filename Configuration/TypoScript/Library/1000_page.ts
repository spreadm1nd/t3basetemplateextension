#########################################################################
###################### Page-Objekt definieren
#########################################################################

page = PAGE
page {
	10 = FLUIDTEMPLATE
	10 {
		layoutRootPath = {$resDir}/Private/Layouts
		partialRootPath = {$resDir}/Private/Partials
		
		file.cObject = CASE
		file.cObject {
			key.data = levelfield:-1, backend_layout_next_level, slide
			key.override.field = backend_layout
			default = TEXT
			default.value = {$resDir}/Private/Templates/00_highline_base_tmpl.html
			
			pagets__1 = TEXT
			pagets__1.value = {$resDir}/Private/Templates/00_highline_base_tmpl.html
			
			pagets__2 = TEXT
			pagets__2.value = {$resDir}/Private/Templates/01_highline_aside_right.tmpl.html
			
			pagets__3 = TEXT
			pagets__3.value = {$resDir}/Private/Templates/00_highline_base_tmpl.html
		}
		
		variables {
			layout = TEXT
			layout {
				data = levelfield:-1,backend_layout_next_level,slide
				override.field = backend_layout
			}
			
			navigation < nav
			metanav < metanav
			inhalt < inhalt
			slider < slider
			projektlogo < projektlogo
			asideright < asideright
			asideleft < asideleft

		}
	}
	
	#Body-Tag modifizieren, damit die Backend-Layouts mit CSS angesprochen werden können    
    bodyTagCObject = CASE
    bodyTagCObject {
    	stdWrap.wrap = <body class="|">
    	key.data = levelfield:-1, backend_layout_next_level, slide
    	key.override.field = backend_layout
    	default = TEXT
    	default.value = highlineBase
    	
    	pagets__1 < .default
    	
    	pagets__2 = TEXT
    	pagets__2.value = highlineBase AsideRight
    	
    	pagets__3 = TEXT
    	pagets__3.value = highlineLandingPage

	}
	
	
	#Ein Favicon einfügen
	shortcutIcon = {$resDir}/Public/Icons/favicon.ico
	
}