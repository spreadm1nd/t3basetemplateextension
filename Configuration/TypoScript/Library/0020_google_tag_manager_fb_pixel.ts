page.headerData.9997 = TEXT
page.headerData.9997.value (
	<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window, document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '1741311722618552');
		fbq('track', 'PageView');
	</script>
)

page.headerData.9998 = TEXT
page.headerData.9998.value (
	<script type="text/javascript">
		var gaProperty = 'UA-121823831-1';
			var disableStr = 'ga-disable-' + gaProperty;
				if (document.cookie.indexOf(disableStr + '=true') > -1) {
					window[disableStr] = true;
				}
				function gaOptout() {
					document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
					window[disableStr] = true;
					alert('Das Tracking durch Google Analytics wurde in Ihrem Browser für diese Website deaktiviert.');
				}
				</script> 
)


page.headerData.9999 = TEXT
page.headerData.9999.value(
	<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-WDNQNSB');</script>
		<!-- End Google Tag Manager -->
)