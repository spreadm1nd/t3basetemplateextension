#########################################################################
###################### Basis-Framework
#########################################################################
config {
	# Typo3-Kommentare im Quelltext der Seite automatisch entfernen
    disablePrefixComment = 1 

	#Mehrsprachigkeit
	linkVars = L
    sys_language_uid = 0
    sys_language_overlay = 0
    sys_language_mode = content_fallback;0
    language = de
    locale_all = de_DE.UTF-8
    htmlTag_setParams = lang="de" dir="ltr" class="de"

    sendCacheHeaders = 1
    
    spamProtectEmailAddresses = -2 
    spamProtectEmailAddresses_atSubst = (at)
    
    
    ### REALURL aktivieren
    absRefPrefix = /
    simulateStaticDocuments = 0
    tx_realurl_enable = 1
    absRelPath = /
    prefixLocalAnchors = all
#     baseURL = https:/highline-cosmetics.de
    
    
    contentObjectExceptionHandler = 0
    
    
    concatenateJs = 0
    compressJs = 0

    concatenateCss = 1
    compressCss = 1
}


### english ###
[globalVar = GP:L = 1]
  config.sys_language_uid = 1
  config.language = en
  config.locale_all = en_EN.utf-8
  config.htmlTag_langKey = en
  config.htmlTag_setParams = lang="en" dir="ltr" class="en"

[global]