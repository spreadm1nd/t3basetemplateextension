#########################################################################
###################### Meta-Navigation
#########################################################################
metanav = HMENU
metanav {
  special = directory
  special.value = 1
  
  1 = TMENU
  1 {
    noBlur = 1
    expAll = 1
    collapse = 0
    wrap =<ul id="metanav">|</ul>
    
    NO = 1
    NO{
      allStdWrap.dataWrap = <li class="copyright">©2018 Highline Cosmetics</li><li class="first">|</li>|*|<li>|</li>|*|<li class="last">
      ATagTitle.field = title // subtitle
    }
  }
}
