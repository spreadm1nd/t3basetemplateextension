#########################################################################
###################### Eigene Partials für WS Flexslider
#########################################################################

plugin.tx_wsflexslider { 
	view {
		partialRootPaths {
			200 = {$resDir}/Private/Partials/ws_flexslider/
		}
	}
}
