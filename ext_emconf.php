<?php


$EM_CONF[$_EXTKEY] = [
    'title' => 'Highline 2018 Distribution',
    'description' => '',
    'category' => 'distribution',
    'author' => 'Tim Zimmermann',
    'author_email' => 'tz@spreadyourmind.de',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '7.6.0-9.9.99',
            'fluid_styled_content' => '',
            'recycler' => '',
//            'powermail' => '',
            'realurl' => '',
            'vhs' => '',
            'gridelements' => '',
            'metaseo' =>'',
//            'jh_magnificpopup' =>'',
            'cookie_hint' =>'',
            'workspaces' =>'',
            'ws_flexslider' =>'',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
